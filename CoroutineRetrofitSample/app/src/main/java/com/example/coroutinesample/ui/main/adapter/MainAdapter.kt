package com.example.coroutinesample.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.coroutinesample.R
import com.example.coroutinesample.data.local.entity.User
import com.example.coroutinesample.ui.base.BaseItemListener
import kotlinx.android.synthetic.main.item_layout.view.*

class MainAdapter(
    private val users: ArrayList<User>,
    private var listener: BaseItemListener<User>
) :
    RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    inner class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(data: User) {
            itemView.apply {
                textViewUserName.text = data.name
                textViewUserCompany.text = data.company?.name
                Glide.with(imageViewAvatar.context)
                    .load(data.profile_image)
                    .into(imageViewAvatar)
            }
        }

        init {
            itemView.setOnClickListener {
                listener.onItemClick(users[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        )

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(users[position])
    }

    fun addUsers(users: List<User>) {
        this.users.apply {
            clear()
            addAll(users)
        }
    }

}