package com.example.coroutinesample.data.api

import com.example.coroutinesample.data.local.entity.User

class ApiHelper(private val apiService: ApiService) {

    suspend fun getUsers(): List<User> = apiService.getUsers()
}