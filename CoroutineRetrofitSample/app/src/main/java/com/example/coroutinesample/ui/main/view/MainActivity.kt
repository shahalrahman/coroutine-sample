package com.example.coroutinesample.ui.main.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.coroutinesample.R.layout
import com.example.coroutinesample.data.api.ApiHelper
import com.example.coroutinesample.data.api.RetrofitBuilder
import com.example.coroutinesample.data.local.entity.User
import com.example.coroutinesample.ui.base.BaseItemListener
import com.example.coroutinesample.ui.base.ViewModelFactory
import com.example.coroutinesample.ui.main.adapter.MainAdapter
import com.example.coroutinesample.ui.main.viewmodel.MainViewModel
import com.example.coroutinesample.utils.Status
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BaseItemListener<User> {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        setupViewModel()
        setupUI()
        setupObservers()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(application, ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }

    private fun setupUI() {
        adapter = MainAdapter(arrayListOf(), this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
    }

    private fun setupObservers() {
        Log.d("API", "setupObservers: ")
        viewModel.userData?.observe(this, Observer {

            Log.d("ChooseProfession", "updateStatus: ${it.status}")
            if (it.status == Status.LOADING) {
                progressBar.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
                return@Observer
            }
            if (it.status == Status.ERROR) {
                recyclerView.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                Log.d("API", "setupObservers: Error ${it.message}")
                return@Observer
            }

            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            it.data?.let { users -> retrieveList(users) }
        })

    }

    private fun retrieveList(users: List<User>) {
        Log.d("API", "retrieveList: Size : ${users.size}")
        adapter.apply {
            addUsers(users)
            notifyDataSetChanged()
        }
    }

    override fun onItemClick(item: User) {
        startActivity(
            Intent(this, UserActivity::class.java)
                .putExtra("USER_DATA", item)
        )
    }
}