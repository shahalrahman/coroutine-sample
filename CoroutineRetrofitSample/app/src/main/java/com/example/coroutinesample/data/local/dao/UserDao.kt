package com.example.coroutinesample.data.local.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.example.coroutinesample.data.local.entity.User

@Dao
interface UserDao {

    @Query("SELECT * FROM User_Table ORDER BY id DESC")
    fun getAllUser(): LiveData<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertList(userList: List<User>)

}