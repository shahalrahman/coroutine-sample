package com.example.coroutinesample.utils

import android.util.Patterns

object QuickUtil {
    fun String?.isValidEmail() = Patterns.EMAIL_ADDRESS.matcher(this).matches()
}