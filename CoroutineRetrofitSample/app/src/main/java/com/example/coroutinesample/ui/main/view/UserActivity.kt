package com.example.coroutinesample.ui.main.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.coroutinesample.R
import com.example.coroutinesample.data.local.entity.User
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        setupUI(intent)
    }

    private fun setupUI(intent: Intent?) {
        if (intent == null) {
            finish()
        }
        val userData: User? = intent?.getSerializableExtra("USER_DATA") as User?

        if (userData == null) {
            finish()
        }


        val company = buildString {
            append("a")
            append("b")
        }
        textViewUserName.text = userData!!.name
        textViewUserName.text = userData.username
        textViewEmail.text = userData.email
        textViewPhone.text = userData.phone
        textViewWeb.text = userData.website
        if (!userData.profile_image.isNullOrBlank()) {
            Glide.with(imageViewAvatar.context)
                .load(userData.profile_image)
                .into(imageViewAvatar)
        }

        if (userData.address != null) {
            val address = buildString {
                append(userData.address?.street)
                append(", ")
                append(userData.address?.suite)
                append(", ")
                append(userData.address?.city)
                append(" - ")
                append(userData.address?.zipcode)
            }
            textViewAddress.text = address

        }
        if (userData.company != null) {
            val company = buildString {
                append(userData.company?.name)
                append("\n")
                append(userData.company?.catchPhrase)
                append("\n")
                append(userData.company?.bs)
            }
            textViewUserCompany.text = company

        }
    }
}