package com.example.coroutinesample.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "User_Table")
data class User(
    @PrimaryKey
    var id: Int,
    var name: String? = "",
    var username: String? = "",
    var email: String? = "",
    var profile_image: String? = "",
    @Embedded var address: Address?,
    var phone: String? = "",
    var website: String? = "",
    @Embedded var company: Company?
) : Serializable, Observable()


data class Address(
    var street: String? = "",
    var suite: String? = "",
    var city: String? = "",
    var zipcode: String? = "",
    @Embedded var geo: Geo
) : Serializable, Observable()

data class Geo(
    var lat: String? = "",
    var lng: String? = ""
): Serializable, Observable()

data class Company(
    @ColumnInfo(name = "companyName") var name: String? = "",
    var catchPhrase: String? = "",
    var bs: String? = ""
) : Serializable, Observable()