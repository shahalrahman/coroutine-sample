package com.example.coroutinesample.data.api

import com.example.coroutinesample.data.local.entity.User
import retrofit2.http.GET

interface ApiService {

    @GET("v2/5d565297300000680030a986")
    suspend fun getUsers(): List<User>
}