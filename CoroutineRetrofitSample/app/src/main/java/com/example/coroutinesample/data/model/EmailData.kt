package com.example.coroutinesample.data.model

data class EmailData(
    val emailId: String
)