package com.example.coroutinesample.ui.main.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.coroutinesample.data.local.entity.User
import com.example.coroutinesample.data.repository.MainRepository
import com.example.coroutinesample.utils.Resource
import kotlinx.coroutines.Dispatchers

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {
    var userData: MediatorLiveData<Resource<List<User>>>? = MediatorLiveData()

    init {
        getUsers()
    }

    fun getServerUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getAllUsers()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getUsers() {
        val data = mainRepository.getUsersFromDB()
        userData?.value = Resource.loading(null)
        userData?.addSource(data, Observer {
            if (it != null) {
                userData?.value = Resource.success(it)
                if ((userData?.value as Resource<List<User>>).data.isNullOrEmpty()) {
                    getUsersData()
                }
            }
        })
    }

    fun getUsersData() {
        val data = getServerUsers()
        userData?.value = Resource.loading(null)
        userData?.addSource(data, Observer {
            if (it != null) {
                userData?.value = it
            }
        })
    }


}