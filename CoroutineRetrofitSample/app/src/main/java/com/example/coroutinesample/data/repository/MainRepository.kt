package com.example.coroutinesample.data.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.example.coroutinesample.data.api.ApiHelper
import com.example.coroutinesample.data.local.dao.UserDao
import com.example.coroutinesample.data.local.db.AppDatabase
import com.example.coroutinesample.data.local.entity.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class MainRepository(application: Application, private val apiHelper: ApiHelper) :

    CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    var mDao: UserDao

    init {
        mDao = AppDatabase.getInstance(application).userDao()
    }

    suspend fun getAllUsers(): List<User> {
        Log.d("MainRepository", "getAllUsers")
        val userList: List<User> = withContext(Dispatchers.IO) { apiHelper.getUsers() }
        if (userList.isNullOrEmpty()) {
            Log.d("MainRepository", "userData: No API data")
        } else {
            insertUserToDB(userList)
        }
        return userList
    }

    fun getUsersFromDB() = mDao.getAllUser()

    suspend fun insertUserToDB(data: List<User>) =
        withContext(Dispatchers.IO) { mDao.insertList(data) }

}