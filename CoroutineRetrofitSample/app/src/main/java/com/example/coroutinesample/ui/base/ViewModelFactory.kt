package com.example.coroutinesample.ui.base

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.coroutinesample.data.api.ApiHelper
import com.example.coroutinesample.data.repository.MainRepository
import com.example.coroutinesample.ui.main.viewmodel.MainViewModel

class ViewModelFactory(private val application: Application, private val apiHelper: ApiHelper) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel( MainRepository(application,apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}