package com.example.coroutinesample.ui.base

interface BaseItemListener<T> {
    fun onItemClick(item: T)
}